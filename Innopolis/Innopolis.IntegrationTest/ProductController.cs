﻿using Xunit;
using System.Threading.Tasks;
using FluentAssertions;
using System.Net;
using System.Collections.Generic;
using Innopolis.Core.Model;
using Newtonsoft.Json;
using System.Net.Http;
using Innopolis.Web;
using System.Net.Http.Headers;

namespace Innopolis.IntegrationTest
{
    public class ProductController : IClassFixture<TestWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _httpClient;
        private readonly string productUrl = "api/product";
        private readonly Product testProduct = new Product { Name = "test product", Amount = 5 };

        public ProductController(TestWebApplicationFactory<Startup> factory)
        {
            _httpClient = factory.CreateClient();
        }

        [Fact]        
        public async Task GetProducts()
        {
            var response = await _httpClient.GetAsync(productUrl);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = JsonConvert.DeserializeObject<List<Product>>(await response.Content.ReadAsStringAsync());
            content.Should().BeEmpty();
        }

        [Fact]
        public async Task CreateProduct()
        {
            var response = await AddProduct();

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task GetProduct()
        {
            await AddProduct();

            var response = await _httpClient.GetAsync($"{productUrl}/1");

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = JsonConvert.DeserializeObject<Product>(await response.Content.ReadAsStringAsync());
            content.Id.Should().Be(1);
            content.Name.Should().Be(testProduct.Name);
        }

        [Fact]
        public async Task UpdateProduct()
        {
            var updatedProduct = new Product { Name = "Updated", Amount = 4 };
            var httpContent = new StringContent(JsonConvert.SerializeObject(updatedProduct));
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var putResponse = await _httpClient.PutAsync(productUrl, httpContent);

            putResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            var response = await _httpClient.GetAsync($"{productUrl}/2");
            var content = JsonConvert.DeserializeObject<Product>(await response.Content.ReadAsStringAsync());
            content.Id.Should().Be(2);
            content.Name.Should().Be(testProduct.Name);
        }

        [Fact]
        public async Task DeleteProduct()
        {
            var deleteResponse = await _httpClient.DeleteAsync($"{productUrl}/1");

            deleteResponse.StatusCode.Should().Be(HttpStatusCode.OK);           
        }

        private async Task<HttpResponseMessage> AddProduct()
        {
            var store = JsonConvert.SerializeObject(testProduct);
            var httpContent = new StringContent(store);
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return await _httpClient.PostAsync(productUrl, httpContent);
        }
    }
}
