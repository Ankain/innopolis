﻿using Xunit;
using System.Threading.Tasks;
using FluentAssertions;
using System.Net;
using System.Collections.Generic;
using Innopolis.Core.Model;
using Newtonsoft.Json;
using System.Net.Http;
using Innopolis.Web;
using System.Net.Http.Headers;

namespace Innopolis.IntegrationTest
{
    public class StoreController : IClassFixture<TestWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _httpClient;
        private readonly string storeUrl = "api/store";
        private readonly Store testStore = new Store { Name = "test Store" };

        public StoreController(TestWebApplicationFactory<Startup> factory)
        {
            _httpClient = factory.CreateClient();
        }

        [Fact]
        public async Task GetStores()
        {            
            var response = await _httpClient.GetAsync(storeUrl);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = JsonConvert.DeserializeObject<List<Store>>(await response.Content.ReadAsStringAsync());
            content.Should().BeEmpty();
        }

        [Fact]
        public async Task CreateStore()
        {
            var response = await AddStore();

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task GetStore()
        {
            await AddStore();

            var response = await _httpClient.GetAsync($"{storeUrl}/1");

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = JsonConvert.DeserializeObject<Store>(await response.Content.ReadAsStringAsync());
            content.Id.Should().Be(1);
            content.Name.Should().Be(testStore.Name);
        }

        [Fact]
        public async Task UpdateStore()
        {           
            var updatedStore = new Store { Name = "Updated Store" };
            var httpContent = new StringContent(JsonConvert.SerializeObject(updatedStore));
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var putResponse = await _httpClient.PutAsync(storeUrl, httpContent);

            putResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var response = await _httpClient.GetAsync($"{storeUrl}/2");
            var content = JsonConvert.DeserializeObject<Store>(await response.Content.ReadAsStringAsync());
            content.Id.Should().Be(2);
            content.Name.Should().Be(testStore.Name);
        }

        [Fact]
        public async Task DeleteStore()
        {
            var deleteResponse = await _httpClient.DeleteAsync($"{storeUrl}/1");

            deleteResponse.StatusCode.Should().Be(HttpStatusCode.OK);            
        }

        private async Task<HttpResponseMessage> AddStore()
        {
            var store = JsonConvert.SerializeObject(testStore);
            var httpContent = new StringContent(store);
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return await _httpClient.PostAsync(storeUrl, httpContent);
        }
    }
}
