﻿using Innopolis.Core.Model;
using Innopolis.DataLayer.Repository;
using Innopolis.Services.Interfaces;

namespace Innopolis.Services.Services
{
    public class ProductService : BaseService<Product> , IProductService
    {
        public ProductService(IDbContext context) : base(context)
        { 
        
        }
    }
}
