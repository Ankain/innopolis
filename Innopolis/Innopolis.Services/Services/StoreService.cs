﻿using Innopolis.Core.Model;
using Innopolis.DataLayer.Repository;
using Innopolis.Services.Interfaces;

namespace Innopolis.Services.Services
{
    public class StoreService : BaseService<Store>, IStoreService
    {
        public StoreService(IDbContext context) : base(context)
        {

        }
    }
}
