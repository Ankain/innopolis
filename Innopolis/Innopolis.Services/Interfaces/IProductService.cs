﻿using Innopolis.Core.Model;

namespace Innopolis.Services.Interfaces
{
    public interface IProductService : IBaseService<Product>
    {
    }
}
