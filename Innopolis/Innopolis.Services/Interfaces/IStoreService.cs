﻿using Innopolis.Core.Model;

namespace Innopolis.Services.Interfaces
{
    public interface IStoreService : IBaseService<Store>
    {
    }
}
