﻿using Innopolis.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innopolis.Core.Model
{
    public class AbstractEntity : IBaseEntity
    {
        /// <inheritdoc />
        public long Id { get; set; }
    }
}
