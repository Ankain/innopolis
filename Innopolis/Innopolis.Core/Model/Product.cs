﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Innopolis.Core.Model
{
    public class Product : AbstractEntity
    {
        [Required]
        public string Name { get; set; }

        [Range(0, int.MaxValue)]
        public long Amount { get; set; }

        public long? StoreId { get; set; }

        public virtual Store Store { get; set; }
    }
}
