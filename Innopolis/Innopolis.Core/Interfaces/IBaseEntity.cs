﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innopolis.Core.Interfaces
{
    /// <summary>
    /// Base Entity
    /// </summary>
    public interface IBaseEntity
    {
        /// <summary>
        /// Id
        /// </summary>
        long Id { get; set; }
    }
}
