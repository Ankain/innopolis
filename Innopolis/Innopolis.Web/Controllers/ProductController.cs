﻿using System.Collections.Generic;
using Innopolis.Core.Model;
using Innopolis.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;

namespace Innopolis.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableQuery()]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public IEnumerable<Product> Get()
        {
            return _productService.Get();
        }
        
        [HttpGet("{id}")]
        public Product Get(int id)
        {
            return _productService.GetById(id);
        }
        
        [HttpPost]
        public void Post([FromBody] Product product)
        {
            _productService.Create(product);
        }
        
        [HttpPut]
        public void Put([FromBody] Product product)
        {
            _productService.Update(product);
        }
        
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _productService.Delete(id);
        }
    }
}
