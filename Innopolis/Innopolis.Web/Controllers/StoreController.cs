﻿using System.Collections.Generic;
using Innopolis.Core.Model;
using Innopolis.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;

namespace Innopolis.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableQuery()]
    public class StoreController : ControllerBase
    {
        private readonly IStoreService _storeService;

        public StoreController(IStoreService storeService)
        {
            _storeService = storeService;
        }                

        [HttpGet]
        public IEnumerable<Store> Get()
        {
            return _storeService.Get();
        }

        [HttpGet("{id}")]
        public Store Get(int id)
        {
            return _storeService.GetById(id);
        }

        [HttpPost]
        public void Post([FromBody] Store store)
        {
            _storeService.Create(store);
        }

        [HttpPut]
        public void Put([FromBody] Store store)
        {
            _storeService.Update(store);
        }
        
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _storeService.Delete(id);
        }
    }
}
