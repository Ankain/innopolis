﻿using Microsoft.EntityFrameworkCore;
using Innopolis.Core.Model;

namespace Innopolis.DataLayer.Repository
{
    public interface IDbContext
    {
        /// <summary>
        /// Store table
        /// </summary>
        DbSet<Store> Stores { get; set; }

        /// <summary>
        /// Product table
        /// </summary>
        DbSet<Product> Products { get; set; }

        /// <summary>
        /// Get table with type
        /// </summary>
        /// <typeparam name="TEntity">Type</typeparam>
        /// <returns>Db table</returns>
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        /// <summary>
        /// Save changes
        /// </summary>
        void SaveChanges();        
    }
}
