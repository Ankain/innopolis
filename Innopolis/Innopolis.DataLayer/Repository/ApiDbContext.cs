﻿using Innopolis.Core.Model;
using Microsoft.EntityFrameworkCore;

namespace Innopolis.DataLayer.Repository
{
    public class ApiDbContext : DbContext , IDbContext
    {
        /// <summary>
        /// Init db object
        /// </summary>
        public ApiDbContext(DbContextOptions options) : base(options)
        {

        }       

        /// <inheritdoc />        
        public DbSet<Store> Stores { get; set; }

        /// <inheritdoc />
        public DbSet<Product> Products { get; set; }

        /// <inheritdoc />
        public new void SaveChanges()
        {
            base.SaveChanges();
        }
    }
}
